#![feature(extern_types)]

pub extern crate core;
pub extern crate libc;

extern crate objrs_macros;

pub use objrs_macros::objrs;
pub use runtime::{Class, Id};
pub use crate::runtime::*;

pub mod marker {
pub unsafe trait Class {}
}

pub mod runtime {
    #[link(name = "objc")]
extern "C" {
  pub fn objc_msgSend();
  pub fn objc_msgSendSuper();
  pub fn objc_msgSend_stret();
}

extern crate core;
extern crate libc;

extern "C" {
  type objc_object;
}

#[repr(transparent)]
pub struct Id<T = ()>(core::marker::PhantomData<T>, core::cell::UnsafeCell<objc_object>)
where
  T: ?Sized;
unsafe impl<T> crate::marker::Class for Id<T> where T: ?Sized {}

#[repr(transparent)]
pub struct Class(Id);

}

mod primitive_types {
  pub type U8 = u8;
}

#[allow(non_camel_case_types)]
pub type u8 = primitive_types::U8;

#[derive(Copy, Clone)]
#[repr(C)]
struct Zst;

#[derive(Copy, Clone)]
#[repr(C)]
pub union UninitPtr {
  zst: Zst,
}
