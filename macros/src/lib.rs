#![feature(proc_macro_diagnostic, proc_macro_span, proc_macro_def_site)]

mod class {
use crate::parse::class_attr::Class;
use proc_macro::Diagnostic;
use proc_macro2::{TokenStream};

use quote::{quote};
use syn::{
  parse_quote, Ident, 
};

pub fn pub_item_struct_and_deref_impls(class: &Class) -> TokenStream {
  let objrs_root = &class.objrs;
  let this_field = Ident::new("_objrs_field_this", proc_macro2::Span::call_site().unstable().resolved_at(proc_macro::Span::def_site()).into());
  let generics_field = Ident::new("__objrs_field_generics", proc_macro2::Span::call_site().unstable().resolved_at(proc_macro::Span::def_site()).into());

  let pub_ident = &class.item.ident;
  let generics = &class.item.generics;
  let (_impl_generics, _ty_generics, where_clause) = class.item.generics.split_for_impl();
  let super_ty = class.super_class.clone().unwrap_or_else(|| parse_quote!(#objrs_root::Id));

  let mut phantom_generics = TokenStream::new();
  for ty in class.item.generics.type_params() {
    let ident = &ty.ident;
    phantom_generics.extend(quote!(#ident,));
  }

  let vis = &class.item.vis;
  let struct_token = &class.item.struct_token;
  return quote! {
    #[repr(transparent)]
    #vis #struct_token #pub_ident #generics #where_clause {
      #[doc(hidden)]
      #generics_field: #objrs_root::core::marker::PhantomData<(#phantom_generics)>,
      #[doc(hidden)]
      #this_field: #super_ty,
    }
  };
}

pub fn transform_class(class: Class) -> Result<TokenStream, Diagnostic> {
  let pub_item = pub_item_struct_and_deref_impls(&class);
  let objrs_root = class.objrs;

  let tokens = quote! {
    extern crate #objrs_root;
    #pub_item
  };

  return Ok(tokens.into());
}
}


mod class_impl {
extern crate core;
extern crate proc_macro;
extern crate proc_macro2;

use crate::parse::impl_attr::ImplAttr;
use crate::selector::{parse_selector_method, ObjrsMethod};
use proc_macro::Diagnostic;
use proc_macro2::{TokenStream};
use quote::quote;
use syn::spanned::Spanned;
use syn::{
  parse2, Attribute, ImplItem, ItemImpl, LitStr, Type, Ident
};

struct ClassImpl {
  _link_attribute: Option<Attribute>,
  item: ItemImpl,
  _class_name: LitStr,
  class_methods: Vec<ObjrsMethod>,
  instance_methods: Vec<ObjrsMethod>,
}

fn parse_class_name(ty: &Type) -> Result<LitStr, Diagnostic> {
  let last_segment = match ty {
    Type::Path(path) => Ok(path.path.segments.last()),
    _ => Err("unknown type"),
  };
  
  let error_prefix = "expected path type, found ";
  let note = "the #[objrs(impl)] macro may only be applied to impl blocks for path types (e.g., \
              `foo::bar::Baz`)";
  match last_segment {
    Ok(Some(path_segment)) => {
      let ident = &path_segment.ident;
      return Ok(LitStr::new(&ident.to_string(), ident.span()));
    } 
    Ok(None) => {
      return Err(ty.span().unstable().error("expected identifer at end of type path").note(note));
    }
    Err(msg) => return Err(ty.span().unstable().error([error_prefix, msg].concat()).note(note)),
  }
}

pub fn transform_impl(attr: ImplAttr, input: TokenStream) -> Result<TokenStream, Diagnostic> {
  let mut item;
  match parse2::<ItemImpl>(input) {
    Ok(value) => item = value,
    Err(error) => {
      return Err(
        error
          .span()
          .unstable()
          .error(format!("failed to parse impl item: {}", error.to_string()))
          .note("#[objrs(impl)] may only be applied to a struct's impl block"),
      );
    }
  };

  let  class_name = parse_class_name(&item.self_ty)?;
  let link_attribute: Option<syn::Attribute> = None;
  let force_extern = attr.force_extern || link_attribute.is_some();

  let mut class_methods = vec![];
  let mut instance_methods = vec![];
  let mut non_methods = vec![];
  let objrs_root = Ident::new("__objrs_root", proc_macro2::Span::call_site().unstable().resolved_at(proc_macro::Span::def_site()).into());
  for sub_item in item.items {
    match sub_item {
      ImplItem::Method(method) => {
        let objrs_method = match parse_selector_method(
          method,
          !item.generics.params.is_empty(),
          force_extern && item.trait_.is_some(),
          force_extern,
          &objrs_root,
        )? {
          Ok(objrs_method) => objrs_method,
          Err(original_method) => {
            non_methods.push(ImplItem::Method(original_method));
            continue;
          }
        };

        if objrs_method.is_instance_method {
          instance_methods.push(objrs_method);
        } else {
          class_methods.push(objrs_method);
        }
      }
      _ => non_methods.push(sub_item),
    }
  }

  item.items = non_methods;

  let class_impl = ClassImpl {
    item: item,
    _link_attribute: link_attribute,
    _class_name: class_name,
    class_methods: class_methods,
    instance_methods: instance_methods,
  };

  let class_methods = class_impl.class_methods;
  let instance_methods = class_impl.instance_methods;
  let mut item = class_impl.item;

  let method_to_impl_item = |method: ObjrsMethod| {
    let msg_send = method.msg_send.into_iter().map(ImplItem::Verbatim);
    let msg_recv = method.msg_recv.into_iter().map(ImplItem::Method);
    return Ok(msg_send.chain(msg_recv));
  };
  for method in class_methods.into_iter().chain(instance_methods.into_iter()) {
    item.items.extend(method_to_impl_item(method)?);
  }

  let pub_objrs = attr.objrs.unwrap_or_else(|| Ident::new("objrs", proc_macro2::Span::call_site().unstable().resolved_at(proc_macro::Span::def_site()).into()));
  let tokens = quote! {
    extern crate #pub_objrs as #objrs_root;

    #item
  };

  return Ok(tokens.into());
}
}


mod parse {
  pub mod attr {
    extern crate core;
    extern crate proc_macro;
    extern crate proc_macro2;
    extern crate syn;
    
    use crate::parse::drain_ext::DrainExt;
    use proc_macro::Diagnostic;
    use proc_macro2::{Span};
    use syn::parse::{Parse, ParseStream};
    use syn::{custom_keyword, AttrStyle, Attribute};
    
    pub fn take_objrs_attr(attrs: &mut Vec<Attribute>) -> Result<Option<Attribute>, Diagnostic> {
      let objrs_attr;
      {
        let mut iter = DrainExt::drain(attrs, |attr: &mut Attribute| match attr.style {
          AttrStyle::Outer => return attr.path.is_ident("objrs"),
          _ => return false,
        });
        objrs_attr = iter.next();
      }
    
      return Ok(objrs_attr);
    }
    
    pub trait Key: Parse {
      fn display() -> &'static str;
    }
    
    macro_rules! impl_token_key {
      ($ty:ty) => {
        impl Key for $ty {
          fn display() -> &'static str {
            return <$ty as syn::token::Token>::display();
          }
        }
      };
    }
    
    macro_rules! impl_custom_key {
      ($ident:ident) => {
        custom_keyword!($ident);
        impl Key for $ident {
          fn display() -> &'static str {
            return concat!("`", stringify!($ident), "`");
          }
        }
      };
    }
    
    impl_token_key!(syn::token::Extern);
    impl_token_key!(syn::token::Impl);
    impl_token_key!(syn::token::Super);
    
    impl_custom_key!(category_name);
    impl_custom_key!(class);
    impl_custom_key!(class_name);
    impl_custom_key!(instance);
    impl_custom_key!(ivar);
    impl_custom_key!(name);
    impl_custom_key!(no_impl);
    impl_custom_key!(objrs);
    impl_custom_key!(optional);
    impl_custom_key!(protocol_name);
    impl_custom_key!(root_class);
    impl_custom_key!(selector);
    
    pub trait KeyTuple {
      fn extend(vec: &mut Vec<&'static str>);
      fn parse_or_nop(input: ParseStream) -> Option<Span>;
    }
    
    fn step_to(input: ParseStream, target: ParseStream) -> Span {
      let span = input.cursor().span();
      input
        .step(|cursor| {
          let mut cursor = *cursor;
          while cursor != target.cursor() {
            cursor = cursor.token_tree().unwrap().1;
          }
          return Ok(((), cursor));
        })
        .unwrap();
      return span;
    }
    
    fn parse_or_nop<K: Key>(input: ParseStream) -> Option<Span> {
      let fork = input.fork();
      if fork.parse::<K>().is_err() {
        return None;
      }
      return Some(step_to(input, &fork));
    }
    
    macro_rules! impl_key_tuple {
      ($($ident:ident)*) => {
        impl<$($ident: Key,)*> KeyTuple for ($($ident,)*) {
          fn extend(vec: &mut Vec<&'static str>) {
            vec.extend(&[$(<$ident as Key>::display(),)*]);
          }
    
          fn parse_or_nop(input: ParseStream) -> Option<Span> {
            return None $(.or_else(|| parse_or_nop::<$ident>(input)))* ;
          }
        }
      }
    }
    
    impl_key_tuple!(T0);
    impl_key_tuple!(T0 T1);
    impl_key_tuple!(T0 T1 T2);
    impl_key_tuple!(T0 T1 T2 T3);
    
    pub trait Value {
      fn parse(input: ParseStream, key_span: Span) -> syn::parse::Result<Self>
      where
        Self: Sized;
    
      fn default() -> Option<Self>
      where
        Self: Sized,
      {
        return None;
      }
    }
    
    impl Value for () {
      fn parse(_: ParseStream, _: Span) -> syn::parse::Result<Self> {
        return Ok(());
      }
    }
    
    impl<T: Value> Value for Option<T> {
      fn parse(input: ParseStream, key_span: Span) -> syn::parse::Result<Self> {
        let value: T = <T as Value>::parse(input, key_span)?;
        return Ok(Some(value));
      }
    
      fn default() -> Option<Self>
      where
        Self: Sized,
      {
        return Some(None);
      }
    }
    
    macro_rules! impl_value_eq_parse {
      ($ty:ty) => {
        impl Value for $ty {
          fn parse(input: ParseStream, _: Span) -> syn::parse::Result<Self>
          where
            Self: Sized,
          {
            let _: syn::token::Eq = input.parse()?;
            return input.parse();
          }
        }
      };
    }
    
    impl_value_eq_parse!(syn::LitStr);
    
    pub struct KV<'a> {
      input: ParseStream<'a>,
      expected: Vec<&'static str>,
    }
    
    impl<'a> KV<'a> {
      pub fn new(input: ParseStream<'a>) -> KV<'a> {
        return KV {
          input: input,
          expected: Vec::new(),
        };
      }
    
      pub fn parse<K: Key, V: Value>(&mut self) -> syn::parse::Result<V> {
        return self.parse_one_of::<(K,), V>();
      }
    
      pub fn parse_one_of<K: KeyTuple, V: Value>(&mut self) -> syn::parse::Result<V> {
        if let Some(key_span) = <K as KeyTuple>::parse_or_nop(self.input) {
          self.expected.clear();
          return self.parse_value::<V>(key_span);
        }
    
        return Ok(<V as Value>::default().unwrap());
      }
    
      pub fn eof(&mut self) -> syn::parse::Result<()> {
          return Ok(());
      }
    
      fn parse_value<V: Value>(&mut self, key_span: Span) -> syn::parse::Result<V> {
        let value = V::parse(self.input, key_span)?;
    
        if !self.input.is_empty() {
          let _: syn::token::Comma = self.input.parse()?;
        }
    
        return Ok(value);
      }
    }    
}


pub mod class_attr {
    extern crate proc_macro2;
    extern crate syn;
    
    use proc_macro::Diagnostic;
    use proc_macro2::{Span, TokenStream};
    use syn::parse::{Parse, ParseStream};
    use syn::{parse2, Ident, ItemStruct, LitStr, Expr, TypePath};
    
    pub struct ClassAttr {
      pub class_name: Option<LitStr>,
      pub super_class_name: Option<LitStr>, // Only needed due to the lack of associated extern statics.
      pub super_class: Option<TypePath>,
      pub root_class_name: Option<LitStr>, // Only needed due to the lack of associated extern statics.
    }
    
    struct SuperAttr {
    }
    
    impl crate::parse::attr::Value for SuperAttr {
      fn parse(input: ParseStream, _: Span) -> syn::parse::Result<Self>
      where
        Self: Sized,
      {
        use syn::token::{Eq};
    
        let _: Eq = input.parse()?;
        let _class: TypePath = input.parse()?;
        return Ok(SuperAttr {
        });
      }
    }
    
    impl Parse for ClassAttr {
      fn parse(input: ParseStream) -> syn::parse::Result<Self> {
        use crate::parse::attr::{class, root_class, KV};
    
        let mut kv = KV::new(input);
        kv.parse::<class, _>()?;
        let root_class: Option<()> = kv.parse::<root_class, _>()?;
        let _super_class: Option<SuperAttr> =
          if root_class.is_some() { None } else { kv.parse::<syn::token::Super, _>()? };
        kv.eof()?;
        return Ok(ClassAttr {
          class_name: None,
          super_class_name: None,
          super_class: None,
          root_class_name: None,
        });
      }
    }
    
    pub struct Ivar {
      pub name: LitStr, // TODO: maybe just make this a String...
      pub default: Option<Expr>,
    }
    
    pub struct Class {
      pub objrs: Ident,
      pub class_name: LitStr,
      pub super_class_name: Option<LitStr>, 
      pub super_class: Option<TypePath>,
      pub force_extern: bool,
      pub root_class_name: LitStr,
      pub item: ItemStruct,
      pub ivars: Vec<Ivar>,
    }
    
    impl Class {
      pub fn new(attr: ClassAttr, input: TokenStream) -> Result<Class, Diagnostic> {
        let item;
        match parse2::<ItemStruct>(input) {
          Ok(value) => item = value,
          Err(error) => {
            return Err(
              error
                .span()
                .unstable()
                .error(format!("failed to parse struct: {}", error.to_string()))
                .note("#[objrs(class ...)] must only be applied to a struct item"),
            );
          }
        }
    
        let force_extern = false;
    
        let class_name =
          attr.class_name.unwrap_or_else(|| LitStr::new(&item.ident.to_string(), item.ident.span()));
    
        let ivars = Vec::new();
    
        let root_class_name;
        if let Some(ref name) = attr.root_class_name {
          root_class_name = name.clone();
        } else if attr.super_class.is_some() {
          root_class_name = LitStr::new("NSObject", Span::call_site());
        } else {
          root_class_name = class_name.clone();
        }
    
        let super_class_name;
        if attr.super_class.is_some() && attr.super_class_name.is_none() {
          let ident = &attr.super_class.as_ref().unwrap().path.segments.last().unwrap().ident;
          super_class_name = Some(LitStr::new(&ident.to_string(), ident.span()));
        } else {
          super_class_name = attr.super_class_name;
        }
    
        return Ok(Class {
          objrs: Ident::new("objrs", proc_macro2::Span::call_site().unstable().resolved_at(proc_macro::Span::def_site()).into()),
          class_name: class_name,
          super_class_name: super_class_name,
          super_class: attr.super_class,
          force_extern: force_extern,
          root_class_name: root_class_name,
          item: item,
          ivars: ivars,
        });
      }
    }    
}


mod drain_ext {


    pub trait DrainExt: IntoIterator {
        fn drain<'a, F: FnMut(&mut <Self as IntoIterator>::Item) -> bool>(
          &'a mut self,
          f: F,
        ) -> Drain<'a, <Self as IntoIterator>::Item, F>;
      }
      
      pub struct Drain<'a, T: 'a, F: FnMut(&mut T) -> bool> {
        pred: F,
        end: usize,
        read: usize,
        write: usize,
        vec: &'a mut Vec<T>,
      }
      
      impl<'a, T, F: FnMut(&mut T) -> bool> core::ops::Drop for Drain<'a, T, F> {
        fn drop(&mut self) {
          let unclaimed = self.end - self.read;
          unsafe {
            core::ptr::copy(
              self.vec.get_unchecked_mut(self.read) as *mut _,
              self.vec.get_unchecked_mut(self.write) as *mut _,
              unclaimed,
            );
            self.vec.set_len(self.write + unclaimed);
          }
        }
      }
      
      impl<'a, T, F: FnMut(&mut T) -> bool> core::iter::Iterator for Drain<'a, T, F> {
        type Item = T;
      
        fn next(&mut self) -> Option<Self::Item> {
          let mut iter = self.read;
          while iter != self.end {
            let current = unsafe { self.vec.get_unchecked_mut(iter) };
            if (self.pred)(current) {
              break;
            }
      
            iter += 1;
          }
      
          let skip_count = iter - self.read;
      
          let result;
          if iter != self.end {
            result = Some(unsafe { core::ptr::read(self.vec.get_unchecked_mut(iter) as *mut _) });
            iter += 1;
          } else {
            result = None;
          }
      
          unsafe {
            core::ptr::copy(
              self.vec.get_unchecked_mut(self.read) as *mut _,
              self.vec.get_unchecked_mut(self.write) as *mut _,
              skip_count,
            )
          };
          self.read = iter;
          self.write += skip_count;
      
          return result;
        }
      
        fn size_hint(&self) -> (usize, Option<usize>) {
          return (0, Some(self.end - self.read));
        }
      }
      
      impl<T> DrainExt for Vec<T> {
        fn drain<'a, F: FnMut(&mut T) -> bool>(&'a mut self, f: F) -> Drain<'a, T, F> {
          let len = self.len();
          unsafe {
            self.set_len(0);
          }
      
          return Drain {
            pred: f,
            end: len,
            read: 0,
            write: 0,
            vec: self,
          };
        }
      }      
}


pub mod impl_attr {
    #![allow(unused)]

    use crate::parse::attr::take_objrs_attr;
    use crate::parse::selector_attr::{ItemMethod, Method};
    use proc_macro::Diagnostic;
    use proc_macro2::TokenStream;
    use syn::parse::{Parse, ParseStream};
    use syn::spanned::Spanned;
    use syn::{parse2, Ident, ImplItem, ItemImpl, LitStr, Type};
    
    pub struct ImplAttr {
      pub class_name: Option<LitStr>,
      pub trait_name: Option<LitStr>,
      pub force_extern: bool,
      pub objrs: Option<Ident>,
    }
    
    impl Parse for ImplAttr {
      fn parse(input: ParseStream) -> syn::parse::Result<Self> {
        use crate::parse::attr::{KV};
        use syn::token::{Extern, Impl};
    
        let mut kv = KV::new(input);
        kv.parse::<Impl, _>()?;
        kv.eof()?;
        return Ok(ImplAttr {
          class_name: None,
          trait_name: None,
          force_extern: false,
          objrs: None,
        });
      }
    }
    
    fn parse_class_name(ty: &Type) -> Result<LitStr, Diagnostic> {
      let note = "the #[objrs(impl)] macro may only be applied to impl blocks for path types (e.g., \
                  `foo::bar::Baz`)";
      return Err(ty.span().unstable().error("expected identifer at end of type path").note(note));
    }
    
    pub struct ClassImpl {
      pub force_extern: bool,
      pub objrs: Ident,
      pub class_name: LitStr,
      pub trait_name: Option<LitStr>,
      // pub link_attribute: Option<Attribute>,
      pub item: ItemImpl,
      pub class_methods: Vec<Method>,
      pub instance_methods: Vec<Method>,
    }
    
    impl ClassImpl {
      pub fn new(attr: ImplAttr, input: TokenStream) -> Result<ClassImpl, Diagnostic> {
        let mut item;
        match parse2::<ItemImpl>(input) {
          Ok(value) => item = value,
          Err(error) => {
            return Err(
              error
                .span()
                .unstable()
                .error(format!("failed to parse impl item: {}", error.to_string()))
                .note("#[objrs(impl)] may only be applied to a struct's impl block"),
            );
          }
        };
    
        let class_name =attr.class_name.unwrap();
    
        let mut class_methods = Vec::new();
        let mut instance_methods = Vec::new();
        let mut non_methods = Vec::new();
    
        item.items = non_methods;
    
        return Ok(ClassImpl {
          force_extern: false,
          objrs:Ident::new("objrs", proc_macro2::Span::call_site().unstable().resolved_at(proc_macro::Span::def_site()).into()),
          class_name: class_name,
          trait_name: None,
          item: item,
          class_methods: class_methods,
          instance_methods: instance_methods,
        });
      }
    }    
}


pub mod selector_attr {
    extern crate proc_macro2;
    extern crate syn;
    
    use proc_macro::Diagnostic;
    use proc_macro2::Span;
    use syn::parse::{Parse, ParseStream};
    use syn::{
      ImplItemMethod, LitStr,
    };
    
    pub struct SelectorAttr {
      pub sel: LitStr,
      pub call_super: bool,
      pub no_impl: bool,
      pub optional: Option<Span>,
      pub method_type: MethodType,
    }
    
    #[derive(PartialEq)]
    pub enum MethodType {
      Auto,
    }
    
    impl Parse for SelectorAttr {
      fn parse(input: ParseStream) -> syn::parse::Result<Self> {
        use crate::parse::attr::{selector, KV};
        use syn::parenthesized;
    
        let content;
        let _: syn::token::Paren = parenthesized!(content in input);
        let input = &content;
    
        let mut kv = KV::new(input);
        let sel: LitStr = kv.parse::<selector, _>()?;
        let method_type = MethodType::Auto;
        kv.eof()?;
        return Ok(SelectorAttr {
          sel: sel,
          call_super: false,
          no_impl: true,
          optional: None,
          method_type: method_type,
        });
      }
    }
    
    pub enum ItemMethod {
      Impl(ImplItemMethod),
    }
    
    impl ItemMethod {
    }
    
    pub struct Method {
      pub attr: SelectorAttr,
      pub method: ItemMethod,
      pub is_instance_method: bool,
      pub is_generic: bool,
    }
    
    impl Method {
      pub fn new(attr: SelectorAttr, method: ItemMethod) -> Result<Method, Diagnostic> {
        return Ok(Method {
          attr: attr,
          method: method,
          is_instance_method: true,
          is_generic: false,
        });
      }
    
      pub fn impl_method(&self) -> Option<&ImplItemMethod> {
        match self.method {
          ItemMethod::Impl(ref method) => return Some(method),
        }
      }
    }    
}
}


mod selector {
use crate::parse::attr::take_objrs_attr;
use crate::parse::selector_attr::{ItemMethod, Method, SelectorAttr};
use proc_macro::Diagnostic;
use proc_macro2::{Span, TokenStream};
use quote::{quote, ToTokens};
use syn::{
  parse2, parse_quote, punctuated::Punctuated, token::Comma,
  FnArg, Ident, ImplItemMethod, LitByteStr, LitStr, ReturnType,
  Type,
};

pub struct ObjrsMethod {
  pub msg_send: Option<TokenStream>,
  pub msg_recv: Option<ImplItemMethod>,
  pub selector: SelectorAttr,
  pub is_instance_method: bool,
}

pub fn parse_selector_method(
  mut method: ImplItemMethod,
  _is_generic_class: bool,
  _empty_msg_recv: bool,
  _force_extern: bool,
  objrs_root: &Ident,
) -> Result<Result<ObjrsMethod, ImplItemMethod>, Diagnostic> {
  let selector_attr: SelectorAttr;
  match take_objrs_attr(&mut method.attrs)? {
    Some(objrs_attr) => {
      selector_attr =
        parse2(objrs_attr.tokens).map_err(|e| e.span().unstable().error(e.to_string()))?;
    }
    None => {
      return Ok(Err(method));
    }
  }

  let method = Method::new(selector_attr, ItemMethod::Impl(method))?;

  let msg_recv = None;

  let msg_send = Some(transform_selector(
      &method.attr,
      method.impl_method().cloned().unwrap(),
      false,
      false,
      None,
      objrs_root,
    )?);

  return Ok(Ok(ObjrsMethod {
    msg_send: msg_send,
    msg_recv: msg_recv,
    selector: method.attr,
    is_instance_method: method.is_instance_method,
  }));
}

fn msg_send_fn(
  selector: &LitStr,
  call_super: bool,
  method: &ImplItemMethod,
  name: &Ident,
  inline: &dyn ToTokens,
  is_instance_method: bool,
  is_generic_class: bool,
  objrs_root: &Ident,
) -> Result<TokenStream, Diagnostic> {
  let mut selector_string = selector.value();

  let objc_send;
  let objc_send_stret;
  if call_super {
    objc_send = quote!(objc_msgSendSuper2);
    objc_send_stret = quote!(objc_msgSendSuper2_stret);
  } else {
    objc_send = quote!(objc_msgSend);
    objc_send_stret = quote!(objc_msgSend_stret);
  }

  let meth_name_export_name =
    ["\x01L_OBJC_METH_VAR_NAME_.__objrs_meth.", "", ".", &selector_string].concat();

  let sel_ref_export_name =
    ["\x01L_OBJC_SELECTOR_REFERENCES_.__objrs_sel.", "", ".", &selector_string].concat();

  selector_string.push('\x00');
  let selector = LitByteStr::new(selector_string.as_bytes(), selector.span());
  let selector_len = selector_string.len();

  let empty_tuple = parse_quote!(());
  let return_type = match method.sig.output {
    ReturnType::Default => &empty_tuple,
    ReturnType::Type(_, ref ty) => ty.as_ref(),
  };

  let generics = &method.sig.generics;

  let native_ty = quote!(#objrs_root);
  let ref_hack;
  if !is_generic_class && generics.params.is_empty() {
    ref_hack = quote! {
      let sel = unsafe { #objrs_root::core::ptr::read_volatile(&SEL_REF as *const _) } as *const _;
    };
  } else {
    ref_hack = quote! {
      #[inline(never)]
      fn sel_ref_hack() -> *const [#native_ty::u8; #selector_len] {
        return unsafe { #objrs_root::core::ptr::read_volatile(&SEL_REF as *const _) } as *const _;
      }
      let sel = sel_ref_hack();
    };
  }

  let where_clause = &generics.where_clause;
  let mut inputs = method.sig.inputs.clone();
  let self_arg_type;
  let self_arg_value;
  if !is_instance_method {
    inputs.insert(0, parse_quote!(_: #objrs_root::UninitPtr));
    if call_super {
      self_arg_type = quote!(*mut #objrs_root::runtime::objc_super);
      self_arg_value = quote! {&mut #objrs_root::runtime::objc_super {
        receiver: #objrs_root::core::mem::transmute(Self::__objrs_class_ref()),
        super_class: Self::__objrs_super_meta_ref(),
      } as *mut _};
    } else {
      self_arg_type = quote!(&'static #objrs_root::Class);
      self_arg_value = quote!(Self::__objrs_class_ref());
    }
  } else {
    match method.sig.inputs[0] {
      FnArg::Receiver(ref receiver) => {
        if call_super {
          self_arg_type = quote!(*mut #objrs_root::runtime::objc_super);
          self_arg_value = quote! {&mut #objrs_root::runtime::objc_super {
            receiver: #objrs_root::core::mem::transmute(self),
            super_class: Self::__objrs_super_class_ref(),
          } as *mut _};
        } else {
          let ampersand = receiver.reference.as_ref().map(|(ampersand, _)| ampersand);
          let lifetime = receiver.reference.as_ref().map(|(_, lifetime)| lifetime);
          let mutability = &receiver.mutability;
          let self_token = &receiver.self_token;
          self_arg_type = quote!(#ampersand #lifetime #mutability Self);
          self_arg_value = quote!(#self_token);
        }
      }
      FnArg::Typed(ref pat_ty) => {
        let pat = &pat_ty.pat;
        let ty = &pat_ty.ty;
        if call_super {
          self_arg_type = quote!(*mut #objrs_root::runtime::objc_super);
          self_arg_value = quote! {&mut #objrs_root::runtime::objc_super {
            receiver: #objrs_root::core::mem::transmute(#pat),
            super_class: Self::__objrs_super_class_ref(),
          } as *mut _};
        } else {
          self_arg_type = quote!(#ty);
          self_arg_value = quote!(#pat);
        }
      }
    }
  }
  inputs.insert(1, parse_quote!(_: #objrs_root::UninitPtr));
  let output = &method.sig.output;

  let tail_arg_types: Punctuated<&Type, Comma> = Punctuated::new();
  let tail_arg_values: Punctuated<&Ident, Comma> = Punctuated::new();

  let unsafety = &method.sig.unsafety;

  let msg_send = quote! {
    #[allow(dead_code)]
    #[inline #inline]
    #[doc(hidden)]
    #[allow(non_upper_case_globals)]
    #unsafety extern "C" fn #name #generics(#inputs) #output #where_clause {
      #[link_section = "__TEXT,__objc_methname,cstring_literals"]
      #[export_name = #meth_name_export_name]
      static METH_NAME: [#native_ty::u8; #selector_len] = * #selector;

      #[link_section = "__DATA,__objc_selrefs,literal_pointers,no_dead_strip"]
      #[export_name = #sel_ref_export_name]
      static SEL_REF: &'static [#native_ty::u8; #selector_len] = &METH_NAME;

      #[cfg(target_arch = "x86_64")]
      let msg_send = if #objrs_root::core::mem::size_of::<#return_type>() <= 16 {
        #objrs_root::runtime::#objc_send
      } else {
        #objrs_root::runtime::#objc_send_stret
      };
      #[cfg(target_arch = "aarch64")]
      let msg_send = #objrs_root::runtime::objc_msgSend;

      let msg_send: unsafe extern fn(#self_arg_type, *const [#native_ty::u8; #selector_len], #tail_arg_types) #output = unsafe { #objrs_root::core::mem::transmute(msg_send as *const ()) };

      #ref_hack

      return unsafe { msg_send(#self_arg_value, sel, #tail_arg_values) };
    }
  };

  return Ok(msg_send);
}

pub fn transform_selector(
  attr: &SelectorAttr,
  method: ImplItemMethod,
  _is_generic_class: bool,
  _empty_msg_recv: bool,
  _fn_span: Option<Span>,
  objrs_root: &Ident,
) -> Result<TokenStream, Diagnostic> {
  let inline = quote!();

  let msg_send_name_str = &["__objrs_msg_send_", method.sig.ident.to_string().as_ref()].concat();
  let msg_send_name =  Ident::new(msg_send_name_str, method.sig.ident.span().unstable().resolved_at(proc_macro::Span::def_site()).into());

  let msg_send = msg_send_fn(
    &attr.sel,
    attr.call_super,
    &method,
    &msg_send_name,
    &inline,
    true,
    true,
    objrs_root,
  )?;

  let tokens = quote! {
    #msg_send
  };

  return Ok(tokens.into());
}
}

extern crate core;
extern crate proc_macro;
extern crate proc_macro2;
extern crate quote;
extern crate syn;

use crate::class::transform_class;
use crate::class_impl::transform_impl;
use crate::parse::class_attr::{Class, ClassAttr};
use crate::parse::impl_attr::ImplAttr;
use proc_macro::Diagnostic;
use proc_macro2::TokenStream;
use syn::parse::{Parse, ParseStream};
use syn::parse2;

enum ObjrsAttr {
  Impl(ImplAttr),
  Class(ClassAttr),
}

impl ObjrsAttr {
  fn transform(self, input: TokenStream) -> Result<TokenStream, Diagnostic> {
    match self {
      ObjrsAttr::Impl(attr) => return transform_impl(attr, input),
      ObjrsAttr::Class(attr) => return transform_class(Class::new(attr, input)?),
    }
  }
}

impl Parse for ObjrsAttr {
  fn parse(input: ParseStream) -> syn::parse::Result<Self> {
    use crate::parse::attr::{class, ivar, selector};
    use syn::token::Impl;

    let lookahead = input.lookahead1();
    if lookahead.peek(Impl) {
      return input.parse().map(ObjrsAttr::Impl);
    } else if lookahead.peek(class) {
      return input.parse().map(ObjrsAttr::Class);
    } else if lookahead.peek(selector) {
      return Err(syn::parse::Error::new(
        input.cursor().span(),
        "attribute must be enclosed in an impl block with a #[objrs(impl)] attribute",
      ));
    } else if lookahead.peek(ivar) {
      return Err(syn::parse::Error::new(
        input.cursor().span(),
        "attribute must be enclosed in a struct item with a #[objrs(class)] attribute",
      ));
    }
    return Err(lookahead.error());
  }
}

fn parse_err_to_diagnostic(err: syn::parse::Error) -> Diagnostic {
  return err.span().unstable().error(err.to_string());
}

#[proc_macro_attribute]
pub fn objrs(
  args: proc_macro::TokenStream,
  input: proc_macro::TokenStream,
) -> proc_macro::TokenStream {
  let args: TokenStream = args.into();
  let input: TokenStream = input.into();
  let result = parse2::<ObjrsAttr>(args).map_err(parse_err_to_diagnostic);
  match result.and_then(|attr| attr.transform(input)) {
    Ok(stream) => return stream.into(),
    Err(diagnostic) => {
      diagnostic.emit();
      return proc_macro::TokenStream::new();
    }
  }
}
